#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <string>
class rectangle{
private:
	double width;
	double hieght;

	class coord {
	public:
		coord();
		coord(double xcoord,double ycoord);
		double x;
		double y;
		int get_y_int();
	    int get_x_int();

		//rotate about the centre width/2* rotation eugh maths
	};
public:
	rectangle();
	rectangle(double xpos,double ypos,double xlen,double ylen);
	void update(double xpos,double ypos);
	void move(double xpos,double ypos);
	coord ul;
	coord ur;
	coord ll;
	coord lr;
};
#endif