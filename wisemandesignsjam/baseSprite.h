#ifndef BASESPRITE_H
#define BASESPRITE_H
#include "CSurface.h"
#include "rectangle.h"
#include <SDL.h>
#include <string>
class baseSprite {
public:
	baseSprite();
    baseSprite(std::string spritename,char* image_file); 
	virtual void draw_sprite(SDL_Surface* baselayer);
	virtual void cleanup_sprite();
	void set_hitbox(double x,double y, double h,double w);
	void update_hitbox(double x,double y);
	double getHitBoxX();
	double getHitBoxY();
	std::string get_spriteName();
	void removeWhite();
	void drawSquare();
	//bool update();
	bool render(SDL_Surface* screen);
protected:
	SDL_Surface* sprite_image;
private:
	std::string sprite_name;
	rectangle hitbox;
	//int xpos;
	//int ypos;
};
#endif