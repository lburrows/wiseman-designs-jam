#ifndef PLAYER_H
#define PLAYER_H
#include "block.h"
#include "tower.h"
#include "player.h"
#include "Base.h"
#include <algorithm>

class player:public block
{
public:
	player(std::string spritename,char* image_file,int X,int Y,int order);
	~player();
	void setIMAGEPtrs(SDL_Surface* imageptr1,SDL_Surface* imageptr2,SDL_Surface* imageptr3);


	bool update();
	bool render(SDL_Surface* screen);
	std::string getPlayerID();
	void setPlayerID(std::string id);
	virtual void setYPOS(int Y);
	virtual void setXPOS(int X);

	void isBuilder();
	void isWrecker();
	void isTorcher();
	bool canBuild();
	bool towerBuilder();

	void incTpiont();
	void incTpiont(int n);
	void decTpiont();
	int getTpiont();
	void addPlayerTower(block* pTower);
	void removePlayerTower(block* pTower);
	void fireTowers();

	std::string getBuilderID();

	int playerNum;
private:
	std::vector<block*> towerList;
	base playerBase;
	int towerPionts;
	int destructionTimer;

	std::string playerID;
	bool buildState;
	bool buildTower;

	SDL_Surface* buildIMAGE;
	SDL_Surface* wreckIMAGE;
	SDL_Surface* torchIMAGE;
};
#endif