#include "Trail.h"
trail::trail(int X,int Y,std::string D)
{
	if(D=="up"){
		baseSprite* temp=new baseSprite("a_trail","trial_up.bmp");
		temp->set_hitbox((X*54)+14,(Y*54)+14,25,25);
		temp->removeWhite();
		trailImage=temp;
	}else if(D=="down"){
		baseSprite* temp=new baseSprite("a_trail","trial_down.bmp");
		temp->set_hitbox((X*54)+14,(Y*54)+14,25,25);
		temp->removeWhite();
		trailImage=temp;
	}else if(D=="left"){
		baseSprite* temp=new baseSprite("a_trail","trial_left.bmp");
		temp->set_hitbox((X*54)+14,(Y*54)+14,25,25);
		temp->removeWhite();
		trailImage=temp;
	}else if(D=="right"){
		baseSprite* temp=new baseSprite("a_trail","trial_right.bmp");
		temp->set_hitbox((X*54)+14,(Y*54)+14,25,25);
		temp->removeWhite();
		trailImage=temp;
	}

}

trail::~trail()
{
}
bool trail::render(SDL_Surface* screen){
	trailImage->render(screen);
	return true;
}