#include "baseSprite.h"
baseSprite::baseSprite(){
	sprite_image=NULL;
}
baseSprite::baseSprite(std::string spritestring,char* image_file){
	if((sprite_image = SDL_LoadBMP( image_file ))==NULL){
		sprite_name="error";
		return;
	}	
}
void baseSprite::draw_sprite(SDL_Surface* baselayer){
	CSurface::OnDraw(baselayer,sprite_image,hitbox.ul.get_x_int(),hitbox.ul.get_y_int());
}
void baseSprite::cleanup_sprite(){
	SDL_FreeSurface(sprite_image);
	
}
void baseSprite::set_hitbox(double x,double y, double h,double w){
	rectangle newHbox(x,y,h,w);
	hitbox=newHbox;
}
void baseSprite::update_hitbox(double x,double y){
	hitbox.update(x,y);
}
double baseSprite::getHitBoxX(){
	//returns x coord of the top left most piont 
	return hitbox.ul.x;
}
double baseSprite::getHitBoxY(){
	//returns y coord of the top left most piont 
	return hitbox.ul.y;
}
std::string baseSprite::get_spriteName(){
	return sprite_name;
}
bool baseSprite::render(SDL_Surface* screen){
	draw_sprite(screen);
	return true;
}
void baseSprite::removeWhite(){
	sprite_image=SDL_DisplayFormat( sprite_image);
		//remove white 
		//map colour key
	Uint32 colourkey=SDL_MapRGB(sprite_image->format,0xFF,0xFF,0xFF);
		//set pixels to transparent
	SDL_SetColorKey(sprite_image,SDL_SRCCOLORKEY,colourkey);
}
void baseSprite::drawSquare(){

}