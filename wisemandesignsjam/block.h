#ifndef BLOCK_H
#define BLOCK_H

#include "baseSprite.h"
#include "kaledo.h"
#include "Trail.h"
#include <vector>
#include <string>

class block:public baseSprite
{
public:
	enum States{CLEAR=0,PLAYER,BLOCK,TOWER,BASE,MINE,TORCH};
	enum direc{UP,DOWN,LEFT,RIGHT};

	block(std::string spritename,char* image_file,int X,int Y);
	~block();
	virtual void setIMAGEPtrs(SDL_Surface* imageptr1,SDL_Surface* imageptr2,SDL_Surface* imageptr3);


	virtual bool update();
	bool renderTrails(SDL_Surface*screen);
	bool renderBGround(SDL_Surface* screen);
	virtual bool render(SDL_Surface* screen);

	//bool moveDirection(direc move_d,std::string moveUntil);

	void setUP(block* b);
	void setDOWN(block* b);
	void setLEFT(block* b);
	void setRIGHT(block* b);
	block* getUP();
	block* getDOWN();
	block* getLEFT();
	block* getRIGHT();
	void block::setHasUp(bool isTrue);
	void block::setHasDown(bool isTrue);
	void block::setHasLeft(bool isTrue);
	void block::setHasRight(bool isTrue);

	virtual void setXPOS(int X);
	int getXPOS();
	virtual void setYPOS(int Y);
	int getYPOS();


	bool hasLeft;
	bool hasRight;
	bool hasUp;
	bool hasDown;
	States getState();
	//ability ressponse
	bool setClear();
	bool addTower(int towerType,std::string playerID,SDL_Surface* tImage,direc direction);
	bool addTorch(int torchType,std::string playerID,SDL_Surface* tImage,direc direction);
	bool getIsTower();
	bool getIsTorch();
	void setBase();
	virtual std::string getBuilderID();
	int fire(std::string shooterID);

	bool checkMine();
	void addMine();
private:
	block* up;
	block* down;
	block* left;
	block* right;

	SDL_Surface* blockIMAGE1;
	SDL_Surface* mineBlock;
	SDL_Surface* baseBlock;

	std::string builderID;

	bool isTower;
	direc towerDirection;
	//torch variables
	bool isTorch;
	bool isLit;
	//mine
	bool isMine;
	//base
	bool isBase;
	
protected:

	States state;

	int xpos;
	int ypos;
	double blockhieght;

	std::vector<kaledo*> bGround;
	std::vector<trail*> trails;
	//std::vector<impact*> impacts; 
};

#endif