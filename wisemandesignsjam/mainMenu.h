#ifndef MAINMENU_H
#define MAINMENU_H

#include <vector>

#include <SDL.h>
#include "CSurface.h"
#include "CEvent.h"

//#include "globalstuff.h"
//#include "coord.h"
//#include "rectangle.h"
//#include "sprite.h"

#include "SDL_ttf.h"
#include "SDL_video.h"
#include "SDL_mixer.h"

#include "theGame1.h"
#include "baseSprite.h"


using namespace std;
namespace mM{
	class mainMenu:public CEvent {
	public:
		mainMenu();
		bool oninit();
		int onexecute();
		void onloop();
		void onevent(SDL_Event* Event);
		//void onClick
		void OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode); 
		void OnKeyUp(SDLKey sym, SDLMod mod, Uint16 unicode);
		void OnExit();
		void onrender();
		void oncleanup();

	private:
		//game1::thegame aGame;
		bool runGame1;
		//GAME2 
		//GAME3  IF YOU WANNA ADD MORE GAMES LATER

		enum menuOps{GAME1=0,EXIT};

		baseSprite selector;
		bool moveUp;
		bool moveDown;
		bool upPressed;
		bool downPressed;
		int option;
		bool returnPressed;
		bool doOption;

		//Screen Surface  rmbr build layer from bottom up
		//image surfaces
		SDL_Surface* screen;
		SDL_Surface* background;
		//text surfaces
		SDL_Surface* title;
		SDL_Surface* playGame1;
		SDL_Surface* exit;
		//The font that's going to be used
		TTF_Font *font;
		//The color of the font
		SDL_Color textColor;


		bool running;

		// music sample variables
		Mix_Music *b_music ;//background
		Mix_Chunk *swarm;// on action sound 
	};
}
#endif