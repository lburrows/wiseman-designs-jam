#include "kaledo.h"
kaledo::kaledo(int X,int Y, double hieght)
{
	blockHieght=hieght;
	numOfShapes=3;


	//load first shape
	baseSprite* newshape;
	newshape=new baseSprite("a kaledo","kaledo_v1.bmp");
	newshape->removeWhite();
	double basex=(X*blockHieght)-23;
	double basey=(Y*blockHieght)-23;
	newshape->set_hitbox(basex,basey,100,100);
	baseShape=newshape;
	listOfShapes.push_back(newshape);

	//load second
	newshape=new baseSprite("a kaledo","kaledo_v2.bmp");
	newshape->removeWhite();
	double calcx1=((X*blockHieght)-basex)*1/(numOfShapes);
	calcx1=basex+calcx1;
	double calcy1=((Y*blockHieght)-basey)*1/(numOfShapes);
	calcy1=basey+calcy1;
	newshape->set_hitbox(calcx1,calcy1,75,75);

	listOfShapes.push_back(newshape);
	//load thrid
	newshape=new baseSprite("a kaledo","kaledo_v3.bmp");
	newshape->removeWhite();
	double calcx2=((X*blockHieght)-basex)*2/(numOfShapes);
	calcx2=basex+calcx2;
	double calcy2=((Y*blockHieght)-basey)*2/(numOfShapes);
	calcy2=basey+calcy2;
	newshape->set_hitbox(calcx2,calcy2,50,50);
	listOfShapes.push_back(newshape);
}

kaledo::~kaledo()
{
}
void kaledo::update(int blockX, int blockY){
	//us blocks topleft(x,y) cooridinate to calulate were to draw the other shapes
	//folllows the block
	//if(baseShape==nullptr){
	//	return;
	//}
	int c=0;
	for(auto s:listOfShapes){
		if(s!=baseShape){
			double basex=baseShape->getHitBoxX();
			double basey=baseShape->getHitBoxY();
			int numofshapes=listOfShapes.size();

			double calcx=((blockX*blockHieght)-basex)*c/(numOfShapes);
			calcx=basex+calcx;
			double calcy=((blockY*blockHieght)-basey)*c/(numOfShapes);
			calcy=basey+calcy;
			s->set_hitbox(calcx,calcy,100,100);
		}
		c++;
	}
}
void kaledo::render(SDL_Surface* screen){
	for(auto s:listOfShapes){
		s->render(screen);
	}

}
void kaledo::addshape(int X,int Y,int imgNum){
	//baseSprite* newshape;
	//if(imgNum==1){
	//	newshape=new baseSprite("a kaledo","kaledo_v1.bmp");
	//	newshape->removeWhite();
	//	if(baseShape==nullptr){
	//		double calcx=(X*blockHieght)-23;
	//		double calcy=(Y*blockHieght)-23;
	//		newshape->set_hitbox(calcx,calcy,100,100);
	//		baseShape=newshape;
	//		listOfShapes.push_back(newshape);
	//	}else{
	//		newshape->set_hitbox(0,0,100,100);
	//		listOfShapes.push_back(newshape);
	//	}
	//}else if(imgNum==2){
	//	newshape=new baseSprite("a kaledo","kaledo_v2.bmp");
	//	newshape->removeWhite();
	//	if(baseShape==nullptr){
	//		double calcx=(X*blockHieght)-23;
	//		double calcy=(Y*blockHieght)-23;
	//		newshape->set_hitbox(calcx,calcy,100,100);
	//		baseShape=newshape;
	//		listOfShapes.push_back(newshape);
	//	}else{
	//		newshape->set_hitbox(0,0,100,100);
	//		listOfShapes.push_back(newshape);
	//	}
	//}else if(imgNum==3){
	//	newshape=new baseSprite("a kaledo","kaledo_v3.bmp");
	//	newshape->removeWhite();
	//	if(baseShape==nullptr){
	//		double calcx=(X*blockHieght)-23;
	//		double calcy=(Y*blockHieght)-23;
	//		newshape->set_hitbox(calcx,calcy,100,100);
	//		baseShape=newshape;
	//		listOfShapes.push_back(newshape);
	//	}else{
	//		newshape->set_hitbox(0,0,100,100);
	//		listOfShapes.push_back(newshape);
	//	}
	//}

//	update(X,Y);
}
