#ifndef BASE_H
#define BASE_H

#include "block.h"

class base:public block
{
public:
	base();
	base(std::string spritename,char* image_file,int X,int Y);
	~base();

private:
	std::string playerID;
};

#endif