#ifndef KALEDO_H
#define KALEDO_H

#include <vector>
#include <string>
#include <cmath>
#include "baseSprite.h"
class kaledo
{
public:
	kaledo(int X,int Y,double hieght);
	~kaledo();

	void update(int blockX, int blockY);
	void render(SDL_Surface* screen);

	void addshape(int X,int Y,int imgNum);

private:
	double blockHieght;

	baseSprite* baseShape; //doesnt move
	std::vector<baseSprite*> listOfShapes;
	std::vector<std::string> imagesAddress;
	int numOfShapes;

};

#endif