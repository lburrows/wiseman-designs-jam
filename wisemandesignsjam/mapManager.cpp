#include "mapManager.h"

mapManager::mapManager(int mapX,int mapY)
{
	//load map tile images
	if((buildIMAGE1 = SDL_LoadBMP( "builder_v1.bmp" ))==NULL){
		return;
	}
	if((wreckIMAGE1 = SDL_LoadBMP( "wrecker_v1.bmp" ))==NULL){
		return;
	}
	if((buildIMAGE2 = SDL_LoadBMP( "builder_v2.bmp" ))==NULL){
		return;
	}
	if((wreckIMAGE2 = SDL_LoadBMP( "wrecker_v2.bmp" ))==NULL){
		return;
	}
	if((torcherIMAGE = SDL_LoadBMP( "builder_v2.bmp" ))==NULL){
		return;
	}
	if((tower_up = SDL_LoadBMP( "tower_up.bmp" ))==NULL){
		return;
	}else{
		tower_up=SDL_DisplayFormat( tower_up);
		//remove white 
		//map colour key
		Uint32 colourkey=SDL_MapRGB(tower_up->format,0xFF,0xFF,0xFF);
		//set pixels to transparent
		SDL_SetColorKey(tower_up,SDL_SRCCOLORKEY,colourkey);
	}
	if((tower_down = SDL_LoadBMP( "tower_down.bmp" ))==NULL){
		return;
	}else{
		tower_down=SDL_DisplayFormat( tower_down);
		//remove white 
		//map colour key
		Uint32 colourkey=SDL_MapRGB(tower_down->format,0xFF,0xFF,0xFF);
		//set pixels to transparent
		SDL_SetColorKey(tower_down,SDL_SRCCOLORKEY,colourkey);
	}
	if((tower_left = SDL_LoadBMP( "tower_left.bmp" ))==NULL){
		return;
	}else{
		tower_left=SDL_DisplayFormat( tower_left);
		//remove white 
		//map colour key
		Uint32 colourkey=SDL_MapRGB(tower_left->format,0xFF,0xFF,0xFF);
		//set pixels to transparent
		SDL_SetColorKey(tower_left,SDL_SRCCOLORKEY,colourkey);
	}
	if((tower_right = SDL_LoadBMP( "tower_right.bmp" ))==NULL){
		return;
	}else{
		tower_right=SDL_DisplayFormat( tower_right);
		//remove white 
		//map colour key
		Uint32 colourkey=SDL_MapRGB(tower_right->format,0xFF,0xFF,0xFF);
		//set pixels to transparent
		SDL_SetColorKey(tower_right,SDL_SRCCOLORKEY,colourkey);
	}
	if((base = SDL_LoadBMP( "wrecker_v2.bmp" ))==NULL){
		return;
	}else{
		base=SDL_DisplayFormat( base);
		//remove white 
		//map colour key
		Uint32 colourkey=SDL_MapRGB(base->format,0xFF,0xFF,0xFF);
		//set pixels to transparent
		SDL_SetColorKey(base,SDL_SRCCOLORKEY,colourkey);
	}
	if((mine = SDL_LoadBMP( "tower_v1.bmp" ))==NULL){
		return;
	}else{
		mine=SDL_DisplayFormat( mine);
		//remove white 
		//map colour key
		Uint32 colourkey=SDL_MapRGB(mine->format,0xFF,0xFF,0xFF);
		//set pixels to transparent
		SDL_SetColorKey(mine,SDL_SRCCOLORKEY,colourkey);
	}
	if((torch = SDL_LoadBMP( "crate_v1.bmp" ))==NULL){
		return;
	}else{
		torch=SDL_DisplayFormat( torch);
		//remove white 
		//map colour key
		Uint32 colourkey=SDL_MapRGB(torch->format,0xFF,0xFF,0xFF);
		//set pixels to transparent
		SDL_SetColorKey(torch,SDL_SRCCOLORKEY,colourkey);
	}
	if((blockIMAGE1 = SDL_LoadBMP( "mapTile_v1.bmp" ))==NULL){
		return;
	}else{
		blockIMAGE1=SDL_DisplayFormat( blockIMAGE1);
		//remove white 
		//map colour key
		Uint32 colourkey=SDL_MapRGB(blockIMAGE1->format,0xFF,0xFF,0xFF);
		//set pixels to transparent
		SDL_SetColorKey(blockIMAGE1,SDL_SRCCOLORKEY,colourkey);
	}


	//create map
	maxX=mapX;
	maxY=mapY;
	for(int Y=0;Y<mapY;Y++){
		for(int X=0;X<mapX;X++){
			std::string tag=std::to_string(X)+std::to_string(Y);
			block* temp=new block(tag,"crate_v1.bmp",X,Y);
			temp->setIMAGEPtrs(blockIMAGE1,mine,base);
			gameMap.push_back(temp);
		}
	}
	//pick 16 random squares to be mines{
	srand(time(NULL));
	int pos;
	bool notRepeat=true;
	std::vector<int> minePos;
	for (int i = 0; i < 16; i++)
	{
		while(notRepeat==true){
			notRepeat=true;
			pos=rand() % 81;
			for(auto m:minePos){
				if(m==pos){
					notRepeat=false;
				}
			}
			if(notRepeat==true){
				minePos.push_back(pos);
				gameMap.at(pos)->addMine();
			}
		}
	}
	//sync map allows map tiles to no whats next to them 
	for(int i=0;i<maxX*maxY;i++){
		block* temp=gameMap[i];
		if(i-maxX<0){
			int tot=maxX*maxY;
			int alter=i%maxX;
			temp->setHasUp(false);
			//if(i==0){
			//	temp->setUP(gameMap[(maxX*maxY)-maxX]);
			//}else{
			//	temp->setUP(gameMap[(maxX*maxY)-(i%maxX)]);
			//}
		}else{
			temp->setHasUp(true);
			temp->setUP(gameMap[i-maxX]);
		}
		if(i+maxX>=maxX*maxY){
			temp->setHasDown(false);
			//temp->setDOWN(gameMap[i%maxX]);
		}else{
			temp->setHasDown(true);
			temp->setDOWN(gameMap[i+maxX]);
		}
		if((i%maxX==0)||(i==0)){
			temp->setHasLeft(false);
			//temp->setLEFT(gameMap[i+maxX-1]);
		}else{
			temp->setHasLeft(true);
			temp->setLEFT(gameMap[i-1]);
		}
		if((i+1)%maxX==0){
			temp->setHasRight(false);
			//temp->setRIGHT(gameMap[i-maxX+1]);
		}else{
			temp->setHasRight(true);
			temp->setRIGHT(gameMap[i+1]);
		}
	}


}

mapManager::~mapManager()
{
}
bool mapManager::renderTrails(SDL_Surface* screen){
	for(auto m:gameMap){
		m->renderTrails(screen);
	}
	return true;
}
bool mapManager::renderBG(SDL_Surface* screen){
	for(auto m:gameMap){
		m->renderBGround(screen);
	}
	return true;
}
bool mapManager::render(SDL_Surface* screen){
	for(auto m:gameMap){
		m->render(screen);
	}
	return true;
}
bool mapManager::update(){
	for(auto m:gameMap){
		m->update();
	}
	return true;
}
void mapManager::addplayer(player* aplayer,int X,int Y){
	if(aplayer->playerNum==1){
		aplayer->setIMAGEPtrs(buildIMAGE1,wreckIMAGE1,torcherIMAGE);
	}else if(aplayer->playerNum==2){
		aplayer->setIMAGEPtrs(buildIMAGE2,wreckIMAGE2,torcherIMAGE);
	}
	int position=(maxY*Y)+X;
	//synch self 
	aplayer->setUP(gameMap[position]->getUP());
	aplayer->setDOWN(gameMap[position]->getDOWN());
	aplayer->setLEFT(gameMap[position]->getLEFT());
	aplayer->setRIGHT(gameMap[position]->getRIGHT());
	//sync others withs self 
	aplayer->hasUp=gameMap[position]->hasUp;
	aplayer->hasDown=gameMap[position]->hasDown;
	aplayer->hasLeft=gameMap[position]->hasLeft;
	aplayer->hasRight=gameMap[position]->hasRight;
	block* temp=aplayer->getUP();
	temp->setDOWN(aplayer);
	temp=aplayer->getDOWN();
	temp->setUP(aplayer);
	temp=aplayer->getLEFT();
	temp->setRIGHT(aplayer);
	temp=aplayer->getRIGHT();
	temp->setLEFT(aplayer);
	//add to map
	gameMap.at(position)=aplayer;

	srand(time(NULL));
	int bPos;
	bool isValid=false;
	while(isValid==false){
		isValid=false;
		bPos=rand() % 81;
		if((gameMap.at(bPos)->checkMine()==false)&&(bPos!=position)){
			isValid=true;
			gameMap.at(bPos)->setBase();
		}
	}

	//insert random name generator
	aplayer->setPlayerID("player"+std::to_string(aplayer->playerNum));

	players[aplayer->getPlayerID()]=aplayer;
}
bool mapManager::hasPlayer(std::string id){
	for(auto p:players){
		if(p.first==id){
			return true;
		}
	}
	return false;
}
bool mapManager::playerUp(std::string id){
	if(hasPlayer(id)==true){
		shiftColumnUP(players[id]);
		return true;
	}else{
		return false;
	}
}
bool mapManager::playerDown(std::string id ){
	if(hasPlayer(id)==true){
		shiftColumnDOWN(players[id]);
		return true;
	}else{
		return false;
	}
}
bool mapManager::playerRight(std::string id ){
	if(hasPlayer(id)==true){
		shiftRowRIGHT(players[id]);
		return true;
	}else{
		return false;
	}
}
bool mapManager::playerLeft(std::string id){
	if(hasPlayer(id)==true){
		shiftRowLEFT(players[id]);
		return true;
	}else{
		return false;
	}
}
void mapManager::shiftColumnUP(block* columnBlock){
	block* currentBlock=columnBlock;
	//find bottom
	while(currentBlock->hasDown==true){
		currentBlock=currentBlock->getDOWN();
	}
	block* bottomL=currentBlock->getLEFT();
	block* bottomR=currentBlock->getRIGHT();
	block* bottom=currentBlock;
	//shift up
	while(currentBlock->hasUp==true){
		block* tmp=currentBlock;
		currentBlock=currentBlock->getUP();
		tmp->setYPOS(tmp->getYPOS()-1);
		//link leftside
		tmp->setLEFT(currentBlock->getLEFT());
		if(tmp->hasLeft==true){
			block* tempL=tmp->getLEFT();
			tempL->setRIGHT(tmp);
		}
		//link rightside
		tmp->setRIGHT(currentBlock->getRIGHT());
		if(tmp->hasRight==true){
			block* tempR=tmp->getRIGHT();
			tempR->setLEFT(tmp);
		}
	}
	//loop round
	//current should now be top
	if(currentBlock!=bottom){
		//link with bottoom block with top
		currentBlock->setLEFT(bottomL);
		if(bottomL!=nullptr){
			bottomL->setRIGHT(currentBlock);
		}else{
			currentBlock->hasLeft=false;
		}
		currentBlock->setRIGHT(bottomR);
		if(bottomR!=nullptr){
			bottomR->setLEFT(currentBlock);
		}else{
			currentBlock->hasRight=false;
		}
		currentBlock->setUP(bottom);
		currentBlock->hasUp=true;
		currentBlock->setYPOS(maxY-1);
		bottom->setDOWN(currentBlock);
		bottom->hasDown=true;
		//remove link with top
		block* tmp=currentBlock->getDOWN();
		tmp->setUP(nullptr);
		tmp->hasUp=false;
		currentBlock->setDOWN(nullptr);
		currentBlock->hasDown=false;
	}

}
void mapManager::shiftColumnDOWN(block* columnBlock){
	block* currentBlock=columnBlock;
	//find top
	while(currentBlock->hasUp==true){
		currentBlock=currentBlock->getUP();
	}
	block* topL=currentBlock->getLEFT();
	block* topR=currentBlock->getRIGHT();
	block* top=currentBlock;
	//shift down
	while(currentBlock->hasDown==true){
		block* tmp=currentBlock;
		currentBlock=currentBlock->getDOWN();
		tmp->setYPOS(tmp->getYPOS()+1);
		tmp->setLEFT(currentBlock->getLEFT());
		if(tmp->hasLeft==true){
			block* tempL=tmp->getLEFT();
			tempL->setRIGHT(tmp);
		}
		tmp->setRIGHT(currentBlock->getRIGHT());
		if(tmp->hasRight==true){
			block* tempR=tmp->getRIGHT();
			tempR->setLEFT(tmp);
		}
	}
	//loop round
	//current should now be bottom
	if(currentBlock!=top){
		//link with botoom
		currentBlock->setLEFT(topL);
		if(topL!=nullptr){
			topL->setRIGHT(currentBlock);
		}else{
			currentBlock->hasLeft=false;
		}
		currentBlock->setRIGHT(topR);
		if(topR!=nullptr){
			topR->setLEFT(currentBlock);
		}else{
			currentBlock->hasRight=false;
		}
		currentBlock->setDOWN(top);
		currentBlock->hasDown=true;
		currentBlock->setYPOS(0);
		top->setUP(currentBlock);
		top->hasUp=true;
		//remove link with top
		block* tmp=currentBlock->getUP();
		tmp->setDOWN(nullptr);
		tmp->hasDown=false;
		currentBlock->setUP(nullptr);
		currentBlock->hasUp=false;
	}
}
void mapManager::shiftRowLEFT(block* columnBlock){
	block* currentBlock=columnBlock;
	//find RIGHT
	while(currentBlock->hasRight==true){
		currentBlock=currentBlock->getRIGHT();
	}
	block* farRU=currentBlock->getUP();
	block* farRD=currentBlock->getDOWN();
	block* farR=currentBlock;
	//shift LEFT
	while(currentBlock->hasLeft==true){
		block* tmp=currentBlock;
		currentBlock=currentBlock->getLEFT();
		tmp->setXPOS(tmp->getXPOS()-1);
		tmp->setUP(currentBlock->getUP());
		if(tmp->hasUp==true){
			block* tempU=tmp->getUP();
			tempU->setDOWN(tmp);
		}
		tmp->setDOWN(currentBlock->getDOWN());
		if(tmp->hasDown==true){
			block* tempD=tmp->getDOWN();
			tempD->setUP(tmp);
		}
	}
	//loop round
	//current should now be far left
	if(currentBlock!=farR){
		//link with botoom
		currentBlock->setUP(farRU);
		if(farRU!=nullptr){
			farRU->setDOWN(currentBlock);
		}else{
			currentBlock->hasUp=false;
		}
		currentBlock->setDOWN(farRD);
		if(farRD!=nullptr){
			farRD->setUP(currentBlock);
		}else{
			currentBlock->hasDown=false;
		}
		currentBlock->setLEFT(farR);
		currentBlock->hasLeft=true;
		currentBlock->setXPOS(maxX-1);
		farR->setRIGHT(currentBlock);
		farR->hasRight=true;
		//remove link with top
		block* tmp=currentBlock->getRIGHT();
		tmp->setLEFT(nullptr);
		tmp->hasLeft=false;
		currentBlock->setRIGHT(nullptr);
		currentBlock->hasRight=false;
	}
}
void mapManager::shiftRowRIGHT(block* columnBlock){
	block* currentBlock=columnBlock;
	//find RIGHT
	while(currentBlock->hasLeft==true){
		currentBlock=currentBlock->getLEFT();
	}
	block* farLU=currentBlock->getUP();
	block* farLD=currentBlock->getDOWN();
	block* farL=currentBlock;
	//shift LEFT
	while(currentBlock->hasRight==true){
		block* tmp=currentBlock;
		currentBlock=currentBlock->getRIGHT();
		tmp->setXPOS(tmp->getXPOS()+1);
		tmp->setUP(currentBlock->getUP());
		if(tmp->hasUp==true){
			block* tempU=tmp->getUP();
			tempU->setDOWN(tmp);
		}
		tmp->setDOWN(currentBlock->getDOWN());
		if(tmp->hasDown==true){
			block* tempD=tmp->getDOWN();
			tempD->setUP(tmp);
		}
	}
	//loop round
	//current should now be far left
	if(currentBlock!=farL){
		//link with botoom
		currentBlock->setUP(farLU);
		if(farLU!=nullptr){
			farLU->setDOWN(currentBlock);
		}else{
			currentBlock->hasUp=false;
		}
		currentBlock->setDOWN(farLD);
		if(farLD!=nullptr){
			farLD->setUP(currentBlock);
		}else{
			currentBlock->hasDown=false;
		}
		currentBlock->setRIGHT(farL);
		currentBlock->hasRight=true;
		currentBlock->setXPOS(0);
		farL->setLEFT(currentBlock);
		farL->hasLeft=true;
		//remove link with top
		block* tmp=currentBlock->getLEFT();
		tmp->setRIGHT(nullptr);
		tmp->hasRight=false;
		currentBlock->setLEFT(nullptr);
		currentBlock->hasLeft=false;
	}
}

void mapManager::player_isbuilder(std::string id){
	if(hasPlayer(id)==true){
		players[id]->isBuilder();
	}else{
	}
}
void mapManager::player_isWrecker(std::string id){
	if(hasPlayer(id)==true){
		players[id]->isWrecker();
	}else{
	}
}
void mapManager::player_isTorcher(std::string id){
	if(hasPlayer(id)==true){
		players[id]->isTorcher();
	}else{
	}
}
bool mapManager::modifymap(std::string id,block::direc d){
	//player is wrecker set block to clear else set to tower
	if(hasPlayer(id)==true){
		if(players[id]->canBuild()==false){
			switch (d)
			{
			case block::UP:
				{
					if(players[id]->hasUp==true){
						block* tmp=players[id]->getUP();
						if(tmp->setClear()==true){
							players[id]->incTpiont();
							if(tmp->getIsTower()==true){
								players[tmp->getBuilderID()]->removePlayerTower(tmp);
							}
							return true;
						}
					}else{
						block* tmp=players[id]->getDOWN();
						while(tmp->hasDown==true){
							tmp=tmp->getDOWN();
						}
						if(tmp->setClear()==true){
							players[id]->incTpiont();
							if(tmp->getIsTower()==true){
								players[tmp->getBuilderID()]->removePlayerTower(tmp);
							}
							return true;
						}
					}
					break;
				}
			case block::DOWN:
				{
					if(players[id]->hasDown==true){
						block* tmp=players[id]->getDOWN();
						if(tmp->setClear()==true){
							players[id]->incTpiont();
							if(tmp->getIsTower()==true){
								players[tmp->getBuilderID()]->removePlayerTower(tmp);
							}
							return true;
						}
					}else{
						block* tmp=players[id]->getUP();
						while(tmp->hasUp==true){
							tmp=tmp->getUP();
						}
						if(tmp->setClear()==true){
							players[id]->incTpiont();
							if(tmp->getIsTower()==true){
								players[tmp->getBuilderID()]->removePlayerTower(tmp);
							}
							return true;
						}
					}
					break;
				}
			case block::LEFT:
				{
					if(players[id]->hasLeft==true){
						block* tmp=players[id]->getLEFT();
						if(tmp->setClear()==true){
							players[id]->incTpiont();
							if(tmp->getIsTower()==true){
								players[tmp->getBuilderID()]->removePlayerTower(tmp);
							}
							return true;
						}
					}else{
						block* tmp=players[id]->getRIGHT();
						while(tmp->hasRight==true){
							tmp=tmp->getRIGHT();
						}
						if(tmp->setClear()==true){
							players[id]->incTpiont();
							if(tmp->getIsTower()==true){
								players[tmp->getBuilderID()]->removePlayerTower(tmp);
							}
							return true;
						}
					}
					break;
				}
			case block::RIGHT:
				{
					if(players[id]->hasRight==true){
						block* tmp=players[id]->getRIGHT();
						if(tmp->setClear()==true){
							players[id]->incTpiont();
							if(tmp->getIsTower()==true){
								players[tmp->getBuilderID()]->removePlayerTower(tmp);
							}
							return true;
						}
					}else{
						block* tmp=players[id]->getLEFT();
						while(tmp->hasLeft==true){
							tmp=tmp->getLEFT();
						}
						if(tmp->setClear()==true){
							players[id]->incTpiont();
							if(tmp->getIsTower()==true){
								players[tmp->getBuilderID()]->removePlayerTower(tmp);
							}
							return true;
						}
					}
					break;
				}
			default:
				break;
			}
		}else if(players[id]->getTpiont()>=2){
			if(players[id]->towerBuilder()==true){
				switch (d)
				{
				case block::UP:
					{
						if(players[id]->hasUp==true){
							block* tmp=players[id]->getUP();
							if(tmp->addTower(players[id]->playerNum,id,tower_up,block::UP)==true){
								players[id]->addPlayerTower(tmp);
								players[id]->decTpiont();
								return true;
							}
						}else{
							block* tmp=players[id]->getDOWN();
							while(tmp->hasDown==true){
								tmp=tmp->getDOWN();
							}
							if(tmp->addTower(players[id]->playerNum,id,tower_up,block::UP)==true){
								players[id]->addPlayerTower(tmp);
								players[id]->decTpiont();
								return true;
							}
						}
						break;
					}
				case block::DOWN:
					{
						if(players[id]->hasDown==true){
							block* tmp=players[id]->getDOWN();
							if(tmp->addTower(players[id]->playerNum,id,tower_down,block::DOWN)==true){
								players[id]->addPlayerTower(tmp);
								players[id]->decTpiont();
								return true;
							}
						}else{
							block* tmp=players[id]->getUP();
							while(tmp->hasUp==true){
								tmp=tmp->getUP();
							}
							if(tmp->addTower(players[id]->playerNum,id,tower_down,block::DOWN)==true){
								players[id]->addPlayerTower(tmp);
								players[id]->decTpiont();
								return true;
							}
						}
						break;
					}
				case block::LEFT:
					{
						if(players[id]->hasLeft==true){
							block* tmp=players[id]->getLEFT();
							if(tmp->addTower(players[id]->playerNum,id,tower_left,block::LEFT)==true){
								players[id]->addPlayerTower(tmp);
								players[id]->decTpiont();
								return true;
							}
						}else{
							block* tmp=players[id]->getRIGHT();
							while(tmp->hasRight==true){
								tmp=tmp->getRIGHT();
							}
							if(tmp->addTower(players[id]->playerNum,id,tower_left,block::LEFT)==true){
								players[id]->addPlayerTower(tmp);
								players[id]->decTpiont();
								return true;
							}
						}
						break;
					}
				case block::RIGHT:
					{
						if(players[id]->hasRight==true){
							block* tmp=players[id]->getRIGHT();
							if(tmp->addTower(players[id]->playerNum,id,tower_right,block::RIGHT)==true){
								players[id]->addPlayerTower(tmp);
								players[id]->decTpiont();
								return true;
							}
						}else{
							block* tmp=players[id]->getLEFT();
							while(tmp->hasLeft==true){
								tmp=tmp->getLEFT();
							}
							if(tmp->addTower(players[id]->playerNum,id,tower_right,block::RIGHT)==true){
								players[id]->addPlayerTower(tmp);
								players[id]->decTpiont();
								return true;
							}
						}
						break;
					}
				default:
					return false;
					break;
				}
			}else{
				switch (d)
				{
				case block::UP:
					{
						if(players[id]->hasUp==true){
							block* tmp=players[id]->getUP();
							if(tmp->addTorch(players[id]->playerNum,id,torch,block::UP)==true){
								//players[id]->addPlayerTorch(tmp);
								players[id]->decTpiont();
								return true;
							}
						}else{
							block* tmp=players[id]->getDOWN();
							while(tmp->hasDown==true){
								tmp=tmp->getDOWN();
							}
							if(tmp->addTorch(players[id]->playerNum,id,torch,block::UP)==true){
								//players[id]->addPlayerTorch(tmp);
								players[id]->decTpiont();
								return true;
							}
						}
						break;
					}
				case block::DOWN:
					{
						if(players[id]->hasDown==true){
							block* tmp=players[id]->getDOWN();
							if(tmp->addTorch(players[id]->playerNum,id,torch,block::DOWN)==true){
								//players[id]->addPlayerTorch(tmp);
								players[id]->decTpiont();
								return true;
							}
						}else{
							block* tmp=players[id]->getUP();
							while(tmp->hasUp==true){
								tmp=tmp->getUP();
							}
							if(tmp->addTorch(players[id]->playerNum,id,torch,block::DOWN)==true){
								//players[id]->addPlayerTorch(tmp);
								players[id]->decTpiont();
								return true;
							}
						}
						break;
					}
				case block::LEFT:
					{
						if(players[id]->hasLeft==true){
							block* tmp=players[id]->getLEFT();
							if(tmp->addTorch(players[id]->playerNum,id,torch,block::LEFT)==true){
								//players[id]->addPlayerTorch(tmp);
								players[id]->decTpiont();
								return true;
							}
						}else{
							block* tmp=players[id]->getRIGHT();
							while(tmp->hasRight==true){
								tmp=tmp->getRIGHT();
							}
							if(tmp->addTorch(players[id]->playerNum,id,torch,block::LEFT)==true){
								//players[id]->addPlayerTorch(tmp);
								players[id]->decTpiont();
								return true;
							}
						}
						break;
					}
				case block::RIGHT:
					{
						if(players[id]->hasRight==true){
							block* tmp=players[id]->getRIGHT();
							if(tmp->addTorch(players[id]->playerNum,id,torch,block::RIGHT)==true){
								//players[id]->addPlayerTorch(tmp);
								players[id]->decTpiont();
								return true;
							}
						}else{
							block* tmp=players[id]->getLEFT();
							while(tmp->hasLeft==true){
								tmp=tmp->getLEFT();
							}
							if(tmp->addTorch(players[id]->playerNum,id,torch,block::RIGHT)==true){
								//players[id]->addPlayerTorch(tmp);
								players[id]->decTpiont();
								return true;
							}
						}
						break;
					}
				default:
					return false;
					break;
				}
			}
		}
	}
	return false;
}
void mapManager::playerFire(std::string id){
	if(hasPlayer(id)==true){
		players[id]->fireTowers();
	}else{
	}
}


