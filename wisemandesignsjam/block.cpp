#include "block.h"
block::block(std::string spritename,char* image_file,int X,int Y):baseSprite(spritename,image_file)
{
	state=BLOCK;
	blockhieght=54;
	set_hitbox(X*blockhieght,Y*blockhieght,blockhieght,blockhieght);
	//clearGraphic
	xpos=X;
	ypos=Y;
	up=nullptr;
	down=nullptr;
	left=nullptr;
	right=nullptr;
	hasUp=false;
	hasDown=false;
	hasLeft=false;
	hasRight=false;

	isTower=false;
	isMine=false;
	isTorch=false;
	isBase=false;
}

void block::setIMAGEPtrs(SDL_Surface* imageptr1,SDL_Surface* imageptr2,SDL_Surface* imageptr3){
	blockIMAGE1=imageptr1;
	mineBlock=imageptr2;
	baseBlock=imageptr3;
	sprite_image=blockIMAGE1;
}
bool block::renderTrails(SDL_Surface*screen){
	for(auto T:trails){
		T->render(screen);
	}
	return true;
}
bool block::renderBGround(SDL_Surface* screen){
	for(auto b:bGround){
		b->render(screen);
	}
	return true;
}
bool block::render(SDL_Surface* screen){
	if(state!=CLEAR){
		if(isLit==true){
			if(isMine==true){
				//state=MINE;
				sprite_image=mineBlock;
			}else if(isBase==true){
				//state=BASE;
				sprite_image=baseBlock;
			}
		}else{
			if((isMine==true)||(isBase==true)){
				//state=BLOCK;
				sprite_image=blockIMAGE1;
			}
		}
		baseSprite::render(screen);
	}
	return true;
}
bool block::update(){
	for(auto b:bGround){
		b->update(xpos,ypos);
	}
	set_hitbox(xpos*blockhieght,ypos*blockhieght,blockhieght,blockhieght);
	
	//check if next to torch
	isLit=false;
	if(hasUp==true){
		if(up->getIsTorch()==true){
			isLit=true;
		}
	}else{
		block* tmp=down->getDOWN();
		while (tmp->hasDown==true)
		{
			tmp=tmp->getDOWN();
		}
		if(tmp->getIsTorch()==true){
			isLit=true;
		}
	}
	if(hasDown==true){
		if(down->getIsTorch()==true){
			isLit=true;
		}
	}else{
		block* tmp=up->getUP();
		while (tmp->hasUp==true)
		{
			tmp=tmp->getUP();
		}
		if(tmp->getIsTorch()==true){
			isLit=true;
		}
	}
	if(hasRight==true){
		if(right->getIsTorch()==true){
			isLit=true;
		}
	}else{
		block* tmp=left->getLEFT();
		while (tmp->hasLeft==true)
		{
			tmp=tmp->getLEFT();
		}
		if(tmp->getIsTorch()==true){
			isLit=true;
		}
	}
	if(hasLeft==true){
		if(left->getIsTorch()==true){
			isLit=true;
		}
	}else{
		block* tmp=right->getRIGHT();
		while (tmp->hasRight==true)
		{
			tmp=tmp->getRIGHT();
		}
		if(tmp->getIsTorch()==true){
			isLit=true;
		}
	}
	return true;
}
block::~block()
{
}
void block::setUP(block* b){
	up=b;
}
void block::setDOWN(block* b){
	down=b;
}
void block::setLEFT(block* b){
	left=b;
}
void block::setRIGHT(block* b){
	right=b;
}
void block::setHasUp(bool isTrue){
	hasUp=isTrue;
}
void block::setHasDown(bool isTrue){
	hasDown=isTrue;
}
void block::setHasLeft(bool isTrue){
	hasLeft=isTrue;
}
void block::setHasRight(bool isTrue){
	hasRight=isTrue;
}
block* block::getUP(){
	return up;
}
block* block::getDOWN(){
	return down;
}
block* block::getLEFT(){
	return left;
}
block* block::getRIGHT(){
	return right;
}
void block::setXPOS(int X){
	xpos=X;
}
int block::getXPOS(){
	return xpos;
}
void block::setYPOS(int Y){
	ypos=Y;
}
int block::getYPOS(){
	return ypos;
}	
bool block::setClear(){
	if((state!=CLEAR)&&(state!=TOWER)&&(state!=TORCH)){
		state=CLEAR;
		kaledo* newkaledo=new kaledo(xpos,ypos,blockhieght);
		bGround.push_back(newkaledo);

		isMine=false;
		//isTower=false;
		//bGround->addshape(xpos,ypos,3);
		return true;
	}
	return false;
}
bool block::addTower(int towerType,std::string playerID,SDL_Surface* tImage,direc direction){
	if((state==BLOCK)&&(isMine==false)){
		state=TOWER;
		builderID=playerID;
		isTower=true;
		isTorch=false;
		sprite_image=tImage;
		towerDirection=direction;
		kaledo* newkaledo=new kaledo(xpos,ypos,blockhieght);
		bGround.push_back(newkaledo);
		//bGround->addshape(xpos,ypos,1);
		//tower* newTower=tower();
		//blockTower=newTower;
		return true;
	}else if(isMine==true){
	}
	return false;
}
bool block::addTorch(int towerType,std::string playerID,SDL_Surface* tImage,direc direction){
	if((state==BLOCK)&&(isMine==false)){
		state=TORCH;
		builderID=playerID;
		isTower=false;
		isTorch=true;
		sprite_image=tImage;
		//towerDirection=direction;
		kaledo* newkaledo=new kaledo(xpos,ypos,blockhieght);
		bGround.push_back(newkaledo);
		//bGround->addshape(xpos,ypos,1);
		//tower* newTower=tower();
		//blockTower=newTower;
		return true;
	}
	return false;
}
bool block::getIsTower(){
	return isTower;
}
bool block::getIsTorch(){
	return isTorch;
}
void block::setBase(){
	state=BASE;
	isBase=true;
	isTorch=false;
	isTower=false;
}
std::string block::getBuilderID(){
	return builderID;
}
bool block::checkMine(){
	return isMine;
}
void block::addMine(){
	state=MINE;
	isMine=true;
}
block::States block::getState(){
	return state;

}
int block::fire(std::string shooterID){
	int hitcount=0;
	if(isTower==true){
		switch (towerDirection)
		{
		case block::UP:
			{
			if(hasUp==true){
				block* tmp=up;
				while(tmp->getState()==CLEAR){
					trail* newtrail=new trail(tmp->getXPOS(),tmp->getYPOS(),"up");
					trails.push_back(newtrail);
					tmp=tmp->getUP();
				}
				if(tmp->checkMine()==true){
					tmp->setClear();
					hitcount++;

				}
			}else{
				block* tmp=down;
				while(tmp->hasDown){
					tmp=tmp->getDOWN();
				}
				while(tmp->getState()==CLEAR){
					trail* newtrail=new trail(tmp->getXPOS(),tmp->getYPOS(),"up");
					trails.push_back(newtrail);
					tmp=tmp->getUP();
				}
				if(tmp->checkMine()==true){
					tmp->setClear();
					hitcount++;

				}	
			}
			break;
		}
		case block::DOWN:
			{
			if(hasDown==true){
				block* tmp=down;
				while(tmp->getState()==CLEAR){
					trail* newtrail=new trail(tmp->getXPOS(),tmp->getYPOS(),"down");
					trails.push_back(newtrail);
					tmp=tmp->getDOWN();
				}
				if(tmp->checkMine()==true){{
					tmp->setClear();
					hitcount++;

				}
				}else{
					block* tmp=up;
					while(tmp->hasUp){
						tmp=tmp->getUP();
					}
					while(tmp->getState()==CLEAR){
						trail* newtrail=new trail(tmp->getXPOS(),tmp->getYPOS(),"down");
						trails.push_back(newtrail);
						tmp=tmp->getDOWN();
					}
					if(tmp->checkMine()==true){
						tmp->setClear();
						hitcount++;

					}	
				}
				break;
			}
		case block::LEFT:
			{
			if(hasLeft==true){
				block* tmp=left;
				while(tmp->getState()==CLEAR){
					trail* newtrail=new trail(tmp->getXPOS(),tmp->getYPOS(),"left");
					trails.push_back(newtrail);
					tmp=tmp->getLEFT();
				}
				if(tmp->checkMine()==true){
					tmp->setClear();
					hitcount++;

				}
			}else{
				block* tmp=right;
				while(tmp->hasRight){
					tmp=tmp->getRIGHT();
				}
				while(tmp->getState()==CLEAR){
					trail* newtrail=new trail(tmp->getXPOS(),tmp->getYPOS(),"left");
					trails.push_back(newtrail);
					tmp=tmp->getLEFT();
				}
				if(tmp->checkMine()==true){
					tmp->setClear();
					hitcount++;

				}	
			}
			break;
			}
		case block::RIGHT:
			{
			if(hasRight==true){
				block* tmp=right;
				while(tmp->getState()==CLEAR){
					trail* newtrail=new trail(tmp->getXPOS(),tmp->getYPOS(),"right");
					trails.push_back(newtrail);
					tmp=tmp->getRIGHT();

				}
				if(tmp->checkMine()==true){
					tmp->setClear();
					hitcount++;

				}
			}else{
				block* tmp=left;
				while(tmp->hasLeft){
					tmp=tmp->getLEFT();
				}
				while(tmp->getState()==CLEAR){
					trail* newtrail=new trail(tmp->getXPOS(),tmp->getYPOS(),"right");
					trails.push_back(newtrail);
					tmp=tmp->getRIGHT();
				}
				if(tmp->checkMine()==true){
					tmp->setClear();
					hitcount++;

				}	
			}
			break;
			}
		default:
			break;
			}
		}
	}
		
	//if(hasUp==true){
	//	block* tmp=up;
	//	while(tmp->state==CLEAR){
	//		trail* newtrail=new trail(tmp->getXPOS(),tmp->getYPOS());
	//		trails.push_back(newtrail);
	//		tmp=tmp->getUP();
	//	}
	//	if((tmp->state==TOWER)||(tmp->state==PLAYER)){
	//		if(tmp->getBuilderID()!=shooterID){
	//			tmp->setClear();
	//			hitcount++;
	//		}
	//	}
	//}else{
	//	block* tmp=down;
	//	while(tmp->hasDown){
	//		tmp=tmp->getDOWN();
	//	}
	//	while(tmp->state==CLEAR){
	//		trail* newtrail=new trail(tmp->getXPOS(),tmp->getYPOS());
	//		trails.push_back(newtrail);
	//		tmp=tmp->getUP();
	//	}
	//		if((tmp->state==TOWER)||(tmp->state==PLAYER)){
	//		if(tmp->getBuilderID()!=shooterID){
	//			tmp->setClear();
	//			hitcount++;
	//		}
	//	}	
	//}
	//if(hasDown==true){
	//	block* tmp=down;
	//	while(tmp->state==CLEAR){
	//		trail* newtrail=new trail(tmp->getXPOS(),tmp->getYPOS());
	//		trails.push_back(newtrail);
	//		tmp=tmp->getDOWN();
	//	}
	//	if((tmp->state==TOWER)||(tmp->state==PLAYER)){
	//		if(tmp->getBuilderID()!=shooterID){
	//			tmp->setClear();
	//			hitcount++;
	//		}
	//	}
	//}else{
	//	block* tmp=up;
	//	while(tmp->hasUp){
	//		tmp=tmp->getUP();
	//	}
	//	while(tmp->state==CLEAR){
	//		trail* newtrail=new trail(tmp->getXPOS(),tmp->getYPOS());
	//		trails.push_back(newtrail);
	//		tmp=tmp->getDOWN();
	//	}
	//	if((tmp->state==TOWER)||(tmp->state==PLAYER)){
	//		if(tmp->getBuilderID()!=shooterID){
	//			tmp->setClear();
	//			hitcount++;
	//		}
	//	}	
	//}
	//if(hasLeft==true){
	//	block* tmp=left;
	//	while(tmp->state==CLEAR){
	//		trail* newtrail=new trail(tmp->getXPOS(),tmp->getYPOS());
	//		trails.push_back(newtrail);
	//		tmp=tmp->getLEFT();
	//	}
	//	if((tmp->state==TOWER)||(tmp->state==PLAYER)){
	//		if(tmp->getBuilderID()!=shooterID){
	//			tmp->setClear();
	//			hitcount++;
	//		}
	//	}
	//}else{
	//	block* tmp=right;
	//	while(tmp->hasRight){
	//		tmp=tmp->getRIGHT();
	//	}
	//	while(tmp->state==CLEAR){
	//		trail* newtrail=new trail(tmp->getXPOS(),tmp->getYPOS());
	//		trails.push_back(newtrail);
	//		tmp=tmp->getLEFT();
	//	}
	//	if((tmp->state==TOWER)||(tmp->state==PLAYER)){
	//		if(tmp->getBuilderID()!=shooterID){
	//			tmp->setClear();
	//			hitcount++;
	//		}
	//	}	
	//}
	//if(hasRight==true){
	//	block* tmp=right;
	//	while(tmp->state==CLEAR){
	//		trail* newtrail=new trail(tmp->getXPOS(),tmp->getYPOS());
	//		trails.push_back(newtrail);
	//		tmp=tmp->getRIGHT();
	//	}
	//	if((tmp->state==TOWER)||(tmp->state==PLAYER)){
	//		if(tmp->getBuilderID()!=shooterID){
	//			tmp->setClear();
	//			hitcount++;
	//		}
	//	}
	//}else{
	//	block* tmp=left;
	//	while(tmp->hasLeft){
	//		tmp=tmp->getLEFT();
	//	}
	//	while(tmp->state==CLEAR){
	//		trail* newtrail=new trail(tmp->getXPOS(),tmp->getYPOS());
	//		trails.push_back(newtrail);
	//		tmp=tmp->getRIGHT();
	//	}
	//	if((tmp->state==TOWER)||(tmp->state==PLAYER)){
	//		if(tmp->getBuilderID()!=shooterID){
	//			tmp->setClear();
	//			hitcount++;
	//		}
	//	}	
	//}
	return hitcount;
}
