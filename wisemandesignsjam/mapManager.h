#ifndef MAPMANAGER_H
#define MAPMANAGER_H

#include <map>
#include <vector>
#include <string>
#include <stdlib.h>     
#include <time.h>  


#include "block.h"
#include "player.h"
#include "Base.h"
#include "tower.h"

class mapManager
{
public:
	//enum direc{UP,DOWN,LEFT,RIGHT};

	mapManager(int mapX,int mapY);
	~mapManager();

	bool update();
	bool render(SDL_Surface* screen);
	bool renderTrails(SDL_Surface* screen);
	bool renderBG(SDL_Surface* screen);
	void addplayer(player* aplayer,int x,int Y);
	bool hasPlayer(std::string id);
	//moves
	bool playerUp(std::string id);
	bool playerDown(std::string id);
	bool playerRight(std::string id);
	bool playerLeft(std::string id);
	void shiftColumnUP(block* columnBlock);
	void shiftColumnDOWN(block* columnBlock);
	void shiftRowLEFT(block* columnBlock);
	void shiftRowRIGHT(block* columnBlock);

	void player_isbuilder(std::string id);
	void player_isWrecker(std::string id);
	void player_isTorcher(std::string id);
	bool modifymap(std::string id,block::direc d);

	void playerFire(std::string id);
private:
	std::vector<block*> gameMap;
	std::map<std::string,player*>players;
	//board dimensions
	int maxX;
	int maxY;

	SDL_Surface* blockIMAGE1;
	SDL_Surface* blockIMAGE2;
	//tower
	SDL_Surface* tower_up;
	SDL_Surface* tower_down;
	SDL_Surface* tower_left;
	SDL_Surface* tower_right;
	//torch
	SDL_Surface* torch;
	//mine
	SDL_Surface* mine;
	//	base block
	SDL_Surface* base;
	//player graphics
	SDL_Surface* buildIMAGE1;
	SDL_Surface* wreckIMAGE1;
	SDL_Surface* buildIMAGE2;
	SDL_Surface* wreckIMAGE2;
	SDL_Surface* torcherIMAGE;

};
#endif