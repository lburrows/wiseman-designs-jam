#ifndef GAME1_H
#define GAME1_H

#include <vector>

#include <SDL.h>
#include "CSurface.h"
#include "CEvent.h"

#include "player.h"
#include "mapManager.h"

//#include "globalstuff.h"
//#include "coord.h"
//#include "rectangle.h"
//#include "sprite.h"

#include "SDL_ttf.h"
#include "SDL_video.h"
#include "SDL_mixer.h"

using namespace std;
namespace game1{
	class thegame:public CEvent {
	public:
		thegame();
		bool oninit();
		int onexecute();
		void onloop();
		void onevent(SDL_Event* Event);
		void OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode);
		void OnKeyUp(SDLKey sym, SDLMod mod, Uint16 unicode);
		void OnExit();
		void onrender();
		void oncleanup();


	private:
		player* player1;
		//player* player2;
		mapManager* map;


		SDL_Surface* background;
		SDL_Surface* screen;
		SDL_Surface* message;
		SDL_Surface* dead_message;
		SDL_Surface* paused_message;
		SDL_Surface* won_message;
		//The font that's going to be used
		TTF_Font *font;

		//The color of the font
		SDL_Color textColor;
		int score;
		char m_score[100];
		bool running;
		bool dead;
		bool won;
		Mix_Music *b_music ;
		Mix_Chunk *swarm;

		//controls
		//--movement
		bool upPressed;
		bool downPressed;
		bool leftPressed;
		bool rightPressed;
		bool moveUp;
		bool moveDown;
		bool moveLeft;
		bool moveRight;
		//--abilitie change
		bool pressed_2;
		bool towerMODE_2;
		bool pressed_1;
		bool WreckMODE_1;
		bool pressed_3;
		bool torchMODE_3;
		//--targetSelect
		bool Wpressed;
		bool Apressed;
		bool Spressed;
		bool Dpressed;
		bool WupSelect;
		bool AleftSelect;
		bool SdownSelect;
		bool DrightSelect;
		//pause
		bool Ppressed;
		bool Ppause;

		bool Opressed;
		bool OscreenShot;

		//turret fire
		bool SPACEpressed;
		bool SPACEfire;

		//switch player stuff
		//bool changePlayer;
		std::string currentPlayer;
	};
}
#endif