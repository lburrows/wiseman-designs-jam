#include "theGame1.h"
namespace game1{
	thegame::thegame()
	{
		//The font that's going to be used
		font = NULL;

		//The color of the font
		SDL_Color theColor = { 255, 255, 255 };
		textColor = theColor;

		message = NULL;
		dead_message=NULL;
		paused_message=NULL;
		won_message=NULL;

		screen = NULL;
		background = NULL;
		score=0;
		running=true;
		dead=false;

		//numofbullets=0;
		//numofmobs=0;


		b_music=NULL;
		swarm=NULL;
	}
	bool thegame::oninit()
	{
		if (SDL_Init( SDL_INIT_VIDEO )<0){
			return false;
		}//can use init_everything, but this is sufficient

		//basic size and other data
		if ((screen = SDL_SetVideoMode( 640, 480, 32, SDL_SWSURFACE ))==NULL){
			return false;
		}

		//read the file into a surface
		if ((background = SDL_LoadBMP( "background_v1.bmp" ))==NULL){
			return false;
		} 
		//read the file into a surface in a sprite class
		player1=new player("player1","builder_v1.bmp",2,2,1);
		//player2=new player("player2","builder_v2.bmp",6,6,2);
		map=new mapManager(9,9);
		map->addplayer(player1,2,2);
//		map->addplayer(player2,6,6);


		//Initialize SDL_ttf
		if( TTF_Init() == -1 ){
			return false;    
		}
		//If there was an error in loading the font
		font = TTF_OpenFont( "Vera-Bold.ttf", 28 );

		if(font == NULL )
		{
			return false;
		}

		//Set the window caption
		SDL_WM_SetCaption( "TTF Test", NULL );

		//preset messages
		dead_message = TTF_RenderText_Blended(font, "you lost", textColor );
		paused_message = TTF_RenderText_Blended(font, "game paused ", textColor );
		won_message = TTF_RenderText_Blended(font, "congratulations you won ", textColor );
		//Initialize SDL_mixer
		if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 )
		{
			return false;    
		}

		//Load the music
		b_music = Mix_LoadMUS( "b_music.wav" );//background music
		//If there was a problem loading the music
		if( b_music == NULL ){
			return false;    
		}
		//Load the sound effects
		swarm = Mix_LoadWAV( "locustswarmloop.wav" );
		//If there was a problem loading the sound effects
		if(  swarm == NULL ){
			return false;    
		}

		moveUp=false;
		moveDown=false;
		moveLeft=false;
		moveRight=false;
		upPressed=false;
		downPressed=false;
		leftPressed=false;
		rightPressed=false;


		pressed_1=false;
		WreckMODE_1=false;
		pressed_2=false;
		towerMODE_2=false;
		pressed_3=false;
		torchMODE_3=false;

		Wpressed=false;
		Apressed=false;
		Spressed=false;
		Dpressed=false;
		WupSelect=false;
		AleftSelect=false;
		SdownSelect=false;
		DrightSelect=false;

		Ppressed=false;
		Ppause=false;

		Opressed=false;
		OscreenShot=false;

		SPACEpressed=false;
		SPACEfire=false;
		//switch player stuff
		//changePlayer=true;

		return true;
	}
	int thegame::onexecute()
	{
		if (oninit()==false){
			return -1;
		}

		SDL_Event Event;
		while (running){
			while(SDL_PollEvent(&Event)){
				onevent(&Event);
			}
			onloop();
			onrender();
		}
		oncleanup();
		return 0;
	}
	void thegame::onevent(SDL_Event* Event)
	{
		// keyboard input is an event
		CEvent::OnEvent(Event);
	}
	void thegame::OnExit(){
		
		running=false;
	}
	void thegame::OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode)
	{
		switch(sym){
		case SDLK_UP:
			//while key is pressed jetpack is on
			if((upPressed==false)&&(Ppause==false)){ //only when key changes from up to down does anything happen
			    upPressed=true;
				moveUp=true;
			}
			if( Mix_PlayChannel( 0, swarm, 0 ) == -1 ){//sfx on
				break;  
			}
			break;
		case SDLK_DOWN:
			//move up true
			if((downPressed==false)&&(Ppause==false)){ //only when key changes from up to down does anything happen
			    downPressed=true;
				moveDown=true;
			}

			break;
		case SDLK_LEFT:
			if((leftPressed==false)&&(Ppause==false)){ //only when key changes from up to down does anything happen
			    leftPressed=true;
				moveLeft=true;
			}
			break;
		case SDLK_RIGHT:
			if((rightPressed==false)&&(Ppause==false)){ //only when key changes from up to down does anything happen
			    rightPressed=true;
				moveRight=true;
			}
			break;
		case SDLK_1:
			if((pressed_1==false)&&(Ppause==false)){ //only when key changes from up to down does anything happen
			    pressed_1=true;
				WreckMODE_1=true;
			}
			break;
		case SDLK_2:
			if((pressed_2==false)&&(Ppause==false)){ //only when key changes from up to down does anything happen
			    pressed_2=true;
				towerMODE_2=true;
			}
			break;
		case SDLK_3:
			if((pressed_3==false)&&(Ppause==false)){ //only when key changes from up to down does anything happen
			    pressed_3=true;
				torchMODE_3=true;
			}
			break;
		case SDLK_w:
			if((Wpressed==false)&&(Ppause==false)){ //only when key changes from up to down does anything happen
			    Wpressed=true;
				WupSelect=true;
			}
			break;
		case SDLK_a:
			if((Apressed==false)&&(Ppause==false)){ //only when key changes from up to down does anything happen
			    Apressed=true;
				AleftSelect=true;
			}
			break;
		case SDLK_s:
			if((Spressed==false)&&(Ppause==false)){ //only when key changes from up to down does anything happen
			    Spressed=true;
				SdownSelect=true;
			}
			break;
		case SDLK_d:
			if((Dpressed==false)&&(Ppause==false)){ //only when key changes from up to down does anything happen
			    Dpressed=true;
				DrightSelect=true;
			}
			break;
		case SDLK_p:
			if(Ppressed==false){ //only when key changes from up to down does anything happen
			    Ppressed=true;
				Ppause=!Ppause;
			}
			//Ppaused= !paused;
			////toggle music on/off when pause button pressed down
			//if( Mix_PausedMusic() == 1 ){//turn sound on
			//	Mix_ResumeMusic();
			//}
			//else{
			//	Mix_PauseMusic();
			//	Mix_HaltChannel(-1);//stop sound effects
			//}
			break;
		case SDLK_o:
			if((Opressed==false)&&(Ppause==true)){ //only when key changes from up to down does anything happen
			    Opressed=true;
				OscreenShot=true;
			}
		case SDLK_SPACE:
			if((SPACEpressed==false)&&(Ppause==false)){ //only when key changes from up to down does anything happen
			    SPACEpressed=true;
				SPACEfire=true;
			}
			break;
		default:
			break;
		}
	}
	void thegame::OnKeyUp(SDLKey sym, SDLMod mod, Uint16 unicode)
	{
		switch(sym){
		case SDLK_UP://when button not pressed jet pack off
			//player1.j_pack.active=false;
			if(upPressed==true){
			    upPressed=false;
			}
			Mix_Pause( 0); //sfx off
			break;
		case SDLK_DOWN:
			//move up true
		    if(downPressed==true){
			    downPressed=false;
			}

			break;
		case SDLK_LEFT:
			if(leftPressed==true){
			    leftPressed=false;
			}
			break;
		case SDLK_RIGHT:
			if(rightPressed==true){
			    rightPressed=false;
			}
			break;
		case SDLK_1:
			if(pressed_1==true){
			    pressed_1=false;
			}
			break;
		case SDLK_2:
			if(pressed_2==true){
			    pressed_2=false;
			}
			break;
		case SDLK_3:
			if(pressed_3==true){
				pressed_3=false;
			}
			break;
		case SDLK_w:
			if(Wpressed==true){
			    Wpressed=false;
			}
			break;
		case SDLK_a:
			if(Apressed==true){
			    Apressed=false;
			}
			break;
		case SDLK_s:
			if(Spressed==true){
			    Spressed=false;
			}
			break;
		case SDLK_d:
			if(Dpressed==true){
			    Dpressed=false;
			}
			break;
		case SDLK_p:
			if(Ppressed==true){
			    Ppressed=false;
			}
			break;
		case SDLK_o:
			if(Opressed==true){
			    Opressed=false;
			}
			break;
		case SDLK_SPACE:
			if(SPACEpressed==true){
			    SPACEpressed=false;
			}
			break;
		case SDLK_ESCAPE:
			running=false;
			break;
		default:
			break;
		}
	}
	void thegame::oncleanup()
	{
		SDL_FreeSurface(background);
//		player1.cleanup_sprite();
		SDL_FreeSurface(screen);


		//Free the sound effects
		Mix_FreeChunk( swarm);
		//Free the music
		Mix_FreeMusic( b_music );

		//free text surface
		SDL_FreeSurface( message );
		SDL_FreeSurface( dead_message );
		SDL_FreeSurface( paused_message );
		SDL_FreeSurface( won_message );

		//Quit SDL_mixer
		Mix_CloseAudio();

		//Close the font that was used
		TTF_CloseFont( font );

		//Quit SDL_ttf
		TTF_Quit();

		SDL_Quit();
	}
	void thegame::onloop()
	{
		//Ifmake sure music is playing
		if( Mix_PlayingMusic() == 0 ){
			//Play the music
			if( Mix_PlayMusic( b_music, -1 ) == -1 ){
				//error
				int error=0;
			}    
		}
		if(Ppause==false){
			//if(changePlayer==true){
			////switch to next player
			//	if(currentPlayer=="player1"){
			//		currentPlayer="player2";
			//		changePlayer=false;
			//	}else{
			//		currentPlayer="player1";
			//		changePlayer=false;
			//	}
			//}
			currentPlayer="player1";

			if(moveUp==true){
				if(map->playerUp(currentPlayer)==false){
					//errorr
				}
				moveUp=false;
				//changePlayer=true;
			}
			if(moveDown==true){
				if(map->playerDown(currentPlayer)==false){
					//error
				}
				moveDown=false;
				//changePlayer=true;
			}
			if(moveLeft==true){
				if(map->playerLeft(currentPlayer)==false){
					//error
				}
				moveLeft=false;
				//changePlayer=true;
			}
			if(moveRight==true){
				if(map->playerRight(currentPlayer)==false){
					//error
				}
				moveRight=false;
				//changePlayer=true;
			}
			if(WreckMODE_1==true){
				map->player_isWrecker(currentPlayer);
				WreckMODE_1=false;
				//changePlayer=false;
			}
			if(towerMODE_2==true){
				map->player_isbuilder(currentPlayer);
				towerMODE_2=false;
				//changePlayer=false;
			}
			if(torchMODE_3==true){
				map->player_isTorcher(currentPlayer);
				torchMODE_3=false;
				//changePlayer=false;
			}
			if(WupSelect==true){
				WupSelect=false;
				if(map->modifymap(currentPlayer,block::UP)==true){
					//changePlayer=true;
				}else{
					//changePlayer=false;
				}
			}
			if(AleftSelect==true){
				AleftSelect=false;
				if(map->modifymap(currentPlayer,block::LEFT)==true){
					//changePlayer=true;
				}else{
					//changePlayer=false;
				}
			}
			if(SdownSelect==true){
				SdownSelect=false;
				if(map->modifymap(currentPlayer,block::DOWN)==true){
					//changePlayer=true;
				}else{
					//changePlayer=false;
				}
			}
			if(DrightSelect==true){
				DrightSelect=false;
				if(map->modifymap(currentPlayer,block::RIGHT)==true){
					//changePlayer=true;
				}else{
					//changePlayer=false;
				}
			}
			if(SPACEfire==true){
				SPACEfire=false;
				map->playerFire(currentPlayer);
					//changePlayer=true;

			}
			map->update();
		}else{
		}
		//update player update map
		//player1.updateypos();


	}
	void thegame::onrender()
	{
		CSurface::OnDraw(screen,background,0,0);
		map->renderTrails(screen);
		map->renderBG(screen);
		if((Ppause==true)&&(OscreenShot==true)){
			if(SDL_SaveBMP(screen, "newbackground.bmp")==-1){
			 bool error=false;
			}
			OscreenShot==false;
		}
		if(Ppause==false){
			map->render(screen);
			player1->render(screen);
		}


		if (Ppause==true){
			CSurface::OnDraw(screen,paused_message, 200, 200);
		}
		if (dead==true){
			CSurface::OnDraw(screen,dead_message, 240, 250);
		}
		if (won==true){
			CSurface::OnDraw(screen,won_message, 20, 250);
		}
		SDL_Flip(screen);
	}
}
