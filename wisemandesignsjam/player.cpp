#include "player.h"
player::player(std::string spritename,char* image_file,int X,int Y,int order):block(spritename,image_file,X,Y){
	buildState=true;
	buildTower=false;
	state=PLAYER;
	playerNum=order;

	towerPionts=2;
}
player::~player(){
}
void player::setIMAGEPtrs(SDL_Surface* imageptr1,SDL_Surface* imageptr2,SDL_Surface* imageptr3){
		buildIMAGE=imageptr1;
	    wreckIMAGE=imageptr2;
		torchIMAGE=imageptr3;
}
std::string player::getPlayerID(){
	return playerID;
}
void player::setPlayerID(std::string id){
	playerID=id;
}
bool player::update(){
	block::update();
	set_hitbox(xpos*blockhieght,ypos*blockhieght,blockhieght,blockhieght);
	return true;
}
bool player::render(SDL_Surface* screen){
	block::render(screen);
	return true;
}
void player::setXPOS(int X){
	xpos=X;
}
void player::setYPOS(int Y){
	ypos=Y;
}

void player::isBuilder(){
	buildState=true;
	buildTower=true;
	sprite_image=buildIMAGE;
	//change graphic
}
void player::isTorcher(){
    buildState=true;
	buildTower=false;
	sprite_image=torchIMAGE;
	//change graphic
}
void player::isWrecker(){
	buildState=false;
	sprite_image=wreckIMAGE;
	//change graphic
}
bool player::canBuild(){
	return buildState;
}
bool player::towerBuilder(){
	return buildTower;
}
void player::incTpiont(){
	towerPionts++;
}
void player::incTpiont(int n){
	towerPionts+=n;
}
void player::decTpiont(){
	towerPionts-=2;
}
int player::getTpiont(){
	return towerPionts;
}
void player::addPlayerTower(block* pTower){
	towerList.push_back(pTower);
}
void player::removePlayerTower(block* pTower){
	std::remove(towerList.begin(),towerList.end(),pTower),towerList.end();
}
void player::fireTowers(){
	for(auto t:towerList){
		if(t->fire(playerID)==true){
			incTpiont();
		}
	}
}
std::string player::getBuilderID(){
	return playerID;
}