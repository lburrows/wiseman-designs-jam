#ifndef TOWER_H
#define TOWER_H
#include "block.h"
#include "bullit.h"

class tower:public block
{
public:
	tower(std::string spritename,char* image_file,int X,int Y);
	~tower();
private:
	std::vector<bullit> bullitList;
	std::string targetType;

};

#endif