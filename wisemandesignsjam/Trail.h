#ifndef TRAIL_H
#define TRAIL_H

#include "baseSprite.h"
class trail
{
public:
	trail(int X,int Y,std::string D);
	~trail();

	bool render(SDL_Surface* screen);

private:
	baseSprite* trailImage;
	int xpos;
	int ypos;
};
#endif